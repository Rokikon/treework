from django import forms
from django.http import HttpResponseRedirect
from mezzanine.pages.page_processors import processor_for
from app.models import CustomPage, AnotherPage
from django.shortcuts import render_to_response


@processor_for(CustomPage)
def custom_form(request, page):
    if request.method == "POST":
        r = request.POST
        return render_to_response("pages/result.html", {
            'count_yes' : int(r.get('count_yes'))
            })
        
    return {}

@processor_for(AnotherPage)
def another_form(request, page):
    if request.method == "POST":
        r = request.POST
        return render_to_response("pages/result.html", {
            'another' : True,
            'count_yes' : int(r.get('count'))
            })
    return render_to_response('pages/another_page.html', {'page': page})