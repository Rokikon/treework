from django.db import models
from mezzanine.pages.models import Page

class CustomPage(Page):
    pass

class AnotherPage(Page):
    pass

class AnotherQuestion(models.Model):
    SELECT_CHOICES = (
        ('0', '0'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
    )

    page = models.ForeignKey(AnotherPage, related_name='select')
    question = models.CharField(max_length=200)
    select = models.CharField(max_length=1, choices=SELECT_CHOICES)

class Question(models.Model):
    page = models.ForeignKey(CustomPage, related_name='question')
    question = models.CharField(max_length=200)