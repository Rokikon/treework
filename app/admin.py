from django.contrib import admin
from mezzanine.core.admin import TabularDynamicInlineAdmin
from mezzanine.pages.admin import PageAdmin
from app.models import CustomPage, Question, AnotherQuestion, AnotherPage


class QuestionInline(TabularDynamicInlineAdmin):
    model = Question
    fields = ("question", )

class CustomPageAdmin(PageAdmin):
    inlines = (QuestionInline, )

admin.site.register(CustomPage, CustomPageAdmin)

class AnotherQuestionInline(TabularDynamicInlineAdmin):
    model = AnotherQuestion
    fields = ("question", )

class AnotherPageAdmin(PageAdmin):
    inlines = (AnotherQuestionInline, )

admin.site.register(AnotherPage, AnotherPageAdmin)

